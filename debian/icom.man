.TH icom 1 "25 September 1996" "" ""
.SH NAME
icom \- remote control for ICOM transceivers and receivers
.SH SYNOPSIS
\fBicom\fR [ \-r radio ] [ \-c channel ] [ \-m mode ] [\-o offset ] [ \-g frequency ] [ \-f file] [ \-adk ]
.SH DESCRIPTION
This program controls ICOM radio transceivers and receivers with the
CI-V option. Most recent ICOM radios already have this option; older
radios can be converted with an appropriate adapter mounted inside the
radio. Up to four CI-V radios can be connected to a single serial port
using a level converter such as the CT-17, which includes a MAX232
chip and not much else.
.PP
The program implements a virtual radio with a bank of memory channels
corresponding to the particular radio model. Each channel can hold
frequency, mode and, in the case of VHF/UHF FM radios, transmit duplex
offset. The virtual radio also includes a variable-frequency
oscillator (VFO) scratch register, which controls the actual receiver
frequency, and several values used to control the tuning and mode
selection functions. Most radios support USB, LSB, AM, CW and RTTY
modes; some support narrow/wide filters and some support FM modes as
well. Additional features that can be controlled on some radios are
antenna selection, scan control, main dial tuning step and several
other functions implemented in the various radio models.
.PP
The actual VFO frequency, mode and transmit duplex offset is
controlled directly by the program. All other registers and memory
channels are accessed via the VFO, which can be loaded from a selected
memory channel, modified and written back to the same or different
channel. When implemented, the actual secondary VFO is controlled by
loading from, or exchanging contents with, the VFO. Other functions,
such as tuning step, tuning rate and compensation for the actual radio
oscillator frequency errors, are performed in software.
.PP
The program knows about most early and late model ICOM radios. The
program can be told which model is present or directed to scan for all
known models and report each one found. A number of diagnostic tests
are performed on the selected radio to determine which options are
present and to initialize to a known state. The program detects
certain anomalistic behavior of some radios and adjusts its operations
to make the behavior conform to the model virtual radio.
.PP
The program operates in one of three modes: keyboard batch, and
keypad. In keyboard mode, commands and arguments are entered from the
keyboard following the \fBicom>\fR prompt, one command per line, and the
complete command set is available. In batch mode, the same commands
and arguments are read from the file specified as an option in the
command line. In keypad mode, commands and arguments are entered from
the keyboard and numeric keypad following the \fB>\fR prompt. In this mode,
most routine keystrokes use the numeric keypad to enter arguments, and
the arguments are followed by a single character representing the
command name. Shortcut arrow keys on the keyboard (not the numeric
keypad) are used to do routine things like tune up/down or
increase/decrease the tuning rate.
.PP
Options on the command line can be used to select the radio model and
set the frequency and mode. Using a suitably crafted Unix \fBcrontab\fR
file, it is possible to tune a radio to different frequencies used by
a shortwave broadcaster throughout the day, for example. With the
\fBminimuf\fR program (available in a separate distribution), it is possible
to build shell scripts that predict the most likely frequencies and
tune the radio accordingly.
.PP
The program includes features to control scanning on some radios and
transmit duplex/split on others. It also includes an optional feature
to control the audio codec on Sun workstations. This feature can be
used in connection with multimedia conferencing programs developed by
the Internet research community. The receiver audio is connected to
the workstation and a MBONE session started with the \fBvat\fR audio tool
running. A remote operator can control the receiver via a telnet
session and use the feature to control the gain, select the input port
and mute the speaker as necessary.
.SH BASIC OPERATION
Most commands and command line options take arguments, although many
arguments can be defaulted. Unsigned floating-point frequency
arguments specify an absolute VFO frequency in MHz, if less than 1000,
and in kHz otherwise. Signed floating-point (preceded by an explicit \fB+\fR
or \fB-\fR character) frequency arguments specify an offset relative to the
current VFO frequency in kHz. Signed or unsigned floating-point offset
arguments specify a transmit duplex, split or beat-frequency
oscillator (BFO) frequency offset in kHz. Signed or unsigned integer
step arguments specify a tuning step in Hz. Unsigned integer channel
arguments specify a memory channel number ranging from 1 to the
highest available in the particular radio. A channel number specified
as '.' means the currently selected channel and '$' the highest
channel available. Other numeric and character string arguments take
values according to the help menus described below.
.PP
Since the most common function is tuning the radio to different
frequencies or scanning a band of frequencies, a compact convention is
provided. In either keyboard, keypad or batch modes, a valid signed or
unsigned floating-point number occurring as the first token on a
command line is an implicit command to set the VFO frequency and may
be followed by a mode specification. In addition, for most commands
and arguments, a question mark '?' appearing in either a command or
argument position causes help information for that command or argument
to be displayed. The information is in two columns - the first shows a
command or option name string, while the second column shows a brief
description of the function.
.PP
The program exits when command line options are processed and the
command line includes any option other than \fB-r\fR, \fB-i\fR or \fB-f\fR. This is
designed for use in shell scripts where all functions can be completed
using only command line options. Keyboard mode is signalled by the
prefix \fBicom>\fR, while keypad mode is signalled by the prefix \fB>\fR and batch
mode has no prefix. Keyboard and batch commands are terminated by the
\fBENTER\fR key on either the keyboard or numeric keypad. Keypad commands
are terminated by a special character depending on the command.
.PP
A radio must be selected before any command which controls it. This
can be done using the \fBradio\fR command in either keyboard or batch modes
and the \fB-r\fR command line option. The behavior of the \fBradio\fR command with
no argument depends on whether a radio has already been selected. If
not, the program scans the CI-V bus for all known radios and for each
one found displays a capability line showing the radio name and tuning
range. If a radio has already been selected, only its capability line
is displayed. The \fBradio\fR command with valid argument can be used at any
time to reselect a different radio, but only one can be active at any
given time.
.PP
The \fBchan\fR command selects the memory channel and transfers its
contents, including the frequency, mode and transmit duplex offset to
the VFO. The \fBfreq\fR command sets the VFO frequency, but does not affect
the mode. Likewise, the \fBmode\fR command sets the mode without affecting
the frequency. For VHF/UHF radios, the \fBduplex\fR command sets the offset;
however, at least with the 271, there appears no obvious means to set
the sign of this offset. The \fBwrite\fR command writes the VFO frequency,
mode and offset to the memory channel, destroying its prior contents.
The \fBclear\fR command clears the memory channel for those radios that
support it.
.PP
Some ICOM radios do not compensate the local oscillator (LO) when the
mode is changed and the BFO is shifted in the passband. This has the
unfortunate effect of detuning the radio by an amount up to 3 kHz when
the mode is changed. When one of these radios are detected during
initialization, the program rewrites the frequency each time the mode
is set. In the case of transceivers with general-coverage receivers,
care should be given to the setting of the GENE-HAM switch. If in the
HAM position, it is not possible to change to a frequency outside the
ham bands. Since the setting of this switch is saved along with the
frequency and mode in a channel and apparently cannot be changed using
radio commands, the setting may have to be done manually.
.SH TUNING OPERATIONS
The keyboard mode can be very awkward when searching a band for
signals, since a new command must be used every time the frequency is
changed. The \fBkeypad\fR command puts the program in keypad mode and
changes the prompt string to ">". In this mode, arguments such as
frequency, tuning step, etc., can be entered directly from the
keyboard and numeric keypad. Of course, the keypad must be in \fBNum Lock\fR
mode for this to work properly. In keypad mode, the arguments are
given first followed by a single character which identifies the
command and terminates input.
.PP
Most ICOM radios tune in 10-Hz steps, while some HF radios tune in
1-Hz steps and some VHF/UHF radios tune in 100-Hz steps. The program
determines the minimum tuning step during initialization and adjusts
the various displays and control ranges accordingly. The easiest way
to tune the radio is using keypad mode and the arrow keys. The \fBUP\fR and
\fBDOWN\fR arrow keys adjust the frequency up or down one step. The \fBLEFT\fR and
\fBRIGHT\fR arrow keys decrease and increase the tuning rate (Hz per step)
respectively. The rate values begin at the minimum tuning step and
extend in 1-2.5-5-10 steps to 5 MHz per step.
.PP
Each press of the \fBUP\fR and \fBDOWN\fR keys displays the current frequency,
mode and either transmit duplex offset or split offset. The display is
also produced by other commands that change these quantities. Each
press of the \fBLEFT\fR and \fBRIGHT\fR keys displays the current tuning rate.
Each press of the \fBENTER\fR key displays the current VFO frequency and
mode. If preceded by a single \fB+\fR or \fB-\fR, the memory channel is
incremented or decremented, respectively, and the contents of that
channel replace the VFO. VFO frequencies can be entered directly using
the keypad \fB+\fR, \fB-\fR, \fB.\fR, digit and \fBENTER\fR keys. With a little practice, it
is easy to scan a band (say with 1-kHz steps in USB) looking for
signals and, when one is found, change to 100-Hz steps to move closer
and then to 10-Hz steps for the final adjustment.
.PP
With the arrow keys, the VFO frequency values are constrained to
follow integral multiples of the rate values. This prevents leaving
the radio on some odd frequency, increasing the tuning rate and
finding the actual tuning steps landing on odd values. When necessary,
The \fBstep\fR keyboard command or \fBs\fR keypad command can be used to change
the tuning step to arbitrary values. This is useful in some
channelized services with non-integral channel spacings, as in the
maritime radio services. The \fBrate\fR keyboard command can be used to set
the rate directly.
.PP
.SH TRANSMIT/RECEIVE SPLIT OPERATION
The 775 and 781 HF transceivers have an auxiliary receiver and VFO
that can be very useful in some operating modes, especially for
working DX and contests. When split mode is in effect, the main VFO is
used for receiving and the auxiliary VFO for transmitting. When
dual-watch is enabled, the audio from both receivers can be combined
in a selectable ratio. The control program supports both features
using keyboard and keypad commands and one of several operating
procedures. Commands are provided to load the transmit VFO from the
receive VFO with selected offset, load the receive VFO from the
transmit VFO with selected offset, and to swap transmit and receive
VFO contents. Upon happening on a DX station, for example, its
frequency can be saved temporarily in the transmit VFO by the \fB0>\fR
keystrokes, then the receive VFO retuned "up ten" to a quiet spot. The
VFO contents are then swapped by the \fB=\fR keystroke. If the DX station
requests calls on frequency, the \fB<\fR keystroke restores the receive VFO
from the transmit VFO. The split can be toggled on and off with the \fB>\fR
keystroke. Many variations in these procedures are possible.
.SH FM DUPLEX OPERATION
Ordinarily, FM repeater operations require that the station receiving
on a frequency transmit at a fixed offset relative to that frequency.
This operation is automatic with most VHF/UHF transceivers and
keyboard commands are provided to read and write the transmit duplex
offset. Keyboard commands are available to specify the duplex offset
and sign, although some VHF/UHF transceivers apparently have no
provision to control the sign of the offset. However, sometimes it is
necessary to listen on the repeater input frequency, instead of the
normal repeater output frequency. To support this feature, the keypad
\fB/\fR keystroke alternates between the repeater input and output
frequencies.
.SH SCANNING OPERATION
For those radios the support scanning, a number of commands are
provided to start the operation in various modes and capture the
frequencies found. The general procdure is to start the scan and wait
for scanning to stop, usually when the squelch is broken. At this
time, the frequency that stopped the scan is displayed and can be
written to a channel with the \fBwrite\fR command.
.PP
The \fBband\fR command can be used to set the low and high band edges for
software scanning. This can be used to search a specific band for
channelized services, such as the broadcasting, aviation and marine
radio bands. Once the band edges and frequency step have been
selected, the keyboard \fBUP\fR and \fBDOWN\fR arrow keys can be used to step the
frequency through the band. When stepped above the high band edge, the
frequency is set to the low band edge. When stepped below the low band
edge, the frequency is set to the high band edge.
.SH OSCILLATOR CALIBRATION
Some ICOM radios, including the 775 and 781, synthesize all oscillator
signals from a single master oscillator. Once the master oscillator
frequency is accurately calibrated, the various LO and BFO signals
will be exactly on frequency. Other ICOM radios synthesize the LO
signal, but use an independent oscillator for the BFO signal. In these
radios, the BFO frequency is shifted using a varactor and a network of
diodes and resistors to generate the necessary BFO frequencies. This
method is not very accurate when remotely tuning the radio to a
narrowband RTTY or packet transmission, for example.
.PP
The program has provisions to compensate for the systematic errors in
both the synthesized LO signal and varactor-switched BFO signals. This
is done by adjusting the VFO frequency to compensate for the
systematic LO error and individual BFO errors. The LO correction is
provided by the \fBcomp\fR command, while the BFO corrections are provided
by the USB, LSB, CW, RTTY, AM and FM mode commands.
.PP
The calibration procedure for single-oscillator radios like the 775
and 781 is simple. Tune the radio to a WWV frequency, preferably the
highest one heard and listen for the 500-Hz or 600-Hz modulation tones
is present. In keypad mode, switch between USB and LSB modes and
adjust the frequency using the \fBUP\fR and \fBDOWN\fR arrow keys until the tones
sound the same. The difference between the indicated frequency and
actual frequency can be read directly from the display or from the
radio. Divide this difference by the actual frequency to calculate the
VFO compensation in PPM.
.PP
For two-oscillator radios, the calibration procedure requires two
frequencies, such as WWV on 10 MHz and 20 MHz, for example. First,
listen for the tones on the 10 MHz frequency and use the above
procedure, but with AM mode substituted for one of the SSB modes.
Then, listen for the tones on the 20 MHz and do the same thing. The LO
error is computed as the difference between the 20 MHz and 10 MHz
differences divided by the difference in the two WWV frequencies, in
this case 10 MHz, is the LO frequency error and the value of the VFO
compensation in PPM.
.PP
The calibration procedure continues for each BFO separately. Starting
with a WWV signal at any frequency, use the above procedure to measure
the difference between the indicated and actual frequencies for each
of the four BFO modes USB, LSB, CW and RTTY. Enter the corrections the
argument to the mode command of the same name. If desired, the CW
and/or RTTY BFO frequencies can be artificially tweaked to move the
center frequency to other than the standard offset. These offsets will
be automatically programmed each time the VFO frequency is changed.
.SH CHANNEL PALETTES
When using this program to operate more than one radio that covers the
same frequency bands and modes, it may be useful to use a standard set
of files, which could be loaded into any of the radios directly from
the file. The program includes the capability to save and restore a
block of memory channels to and from a file using the \fBsave\fR and \fBrestore\fR
commands. This allows channel blocks to be created by other programs
and copied from one radio to another, for example. The information
saved in the file includes the frequency, mode and offset. The program
can also execute a list of commands stored in a file and interpreted
in batch mode.
.PP
However, it often happens that individual radios need slightly
different frequency settings to receive the same frequency
transmission, especially when narrow filters are in use. A common
example is when selecting from a standard set of frequencies provided
by the \fBminimuf\fR program (described elsewhere) and a Unix \fBcron\fR to
automatically tune the radio(s) throughout the day. The standard
frequencies can be maintained in a batch file common to all radios and
loaded by the \fBrestore\fR batch command.
.PP
However, the \fBcron\fR script can provide both the frequency \fB-g\fR and
individual offset \fB-o\fR values specific to each radio on the command
line. The program adds this offset, along with others provided by the
\fBoffset\fR and \fBmode\fR commands, if used, as the frequency is transmitted to
the radio. Usually, the offset is characteristic of the radio, as the
result of BFO frequency error, for example, and can be calibrated
manually in advance.
.PP
Batch mode is designed for cases where a number of radios are to be
cloned or programmed with memory channel data produced by another
program. In principle, a Unix shell script could search an archive for
the current VOA transmission schedules and transmitter locations,
another program determine the propagation model and best frequencies
for the receiver location and current time of day, and then program
the radio(s) with the results.
.SH INTERFACE DEBUGGING
The program communicates with one or more radios using the CI-V
broadcast bus and serial asynchronous protocol. The CT-17 level
converter can be used to interface the CI-V voltage levels (TTL) to
EIA (RS-232) levels, or a homebrew unit can be made from the MAX232 IC
plus a handful of capacitors. The CT-17 supports up to four radios,
but there is no inherent protocol limitation to this number. The CI-V
uses active-low drivers with resistor pullups, so multiple radios can
be connected to the same wire.
.PP
Messages are exchanged in the form of frames beginning with two
preamble bytes (0xfe) and ending with one end of message byte (0xfd).
In order to handle the older radios, the control program operates at
1200 bps; however, it can be compiled to operate at higher speeds.
Each radio model is assigned a unique identifier byte, which can be
changed if necessary. The control program sends a frame with that
identifier and expects a reply, either containing data or a single ACK
(0xfb) or NAK (0xfa). Most functions implemented by the control
program require an exchange of several frames.
.PP
It has been the experience that some radios can occasionally fail to
respond to a command or respond with a mangled frame. Therefore, the
control program includes a good deal of error recovery code and uses
timeouts and retransmissions as necessary. Since the CI-V bus uses a
broadcast architecture, every octet transmitted by the control program
is read back for verification. If the readback fails or no reply is
received after three retransmissions, the operation fails and an error
message is displayed.
.PP
The \fBtrace\fR command can be used to watch the protocol interactions
between the program and radios. The argument \fBbus\fR enables packet trace.
The trace operates from received octets, either a readback of a
transmitted frame preceded by "T:" or a copy of a received frame
preceded by "R:". Each transmission is repeated up to three times in
case of error, after which the operation fails and is reported as an
error. The argument \fBpacket\fR enables bus error messages, which are
normally suppressed until the maximum retry limit is reached.
.SH HISTORY
Written by David L. Mills, W3HCF; this update 25 September 1996. This
manual page was created from the original HTML documentation by Hamish
Moffatt VK3SB <hamish@debian.org> for Debian (but may be used by others).
.SH BUGS
This is a work in progress. Many idiosyncrasies of various ICOM radios
remain to be discovered.
